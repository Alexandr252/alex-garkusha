<?php
$installer = $this;
$installer->startSetup();

/**
 *
 * entity_id (unsignet int, primary, not null)
 *
 * title (varchar 255 not null)
 *
 * image (varchar 255, nullable)
 *
 * content (mediumtext, not null)
 *
 * create Date (timestamp, not null)
 *
 * update date (timestamp, not null)
 *
 * publish (unsigned small int, not null)
 *
 */

$table = $installer->getConnection()
    ->newTable($installer->getTable('aws_news/news'))
    ->addColumn('entity_id',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'identity' => true,
    ), 'Entity ID')
    ->addColumn('title',Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, null, array(
        'nullable' => false,
    ), 'aws_news title')
    ->addColumn('image',Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, null, array(
        'nullable' => true,
    ), 'aws_news image')
    ->addColumn('content',Varien_Db_Ddl_Table::TYPE_TEXT, '16M', null, array(
        'nullable' => false,
    ), 'aws_news content')
    ->addColumn('create_date',Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => false,
    ), 'aws_news create Date')
    ->addColumn('update_date',Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => false,
    ), 'aws_news update Date')
    ->addColumn('publish',Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable' => false,
        'default'   => '0'
    ), 'aws_news publish');

$installer->getConnection()->createTable($table);
$installer->endSetup();